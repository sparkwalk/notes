#  The `Py_BuildValue()` Function

This function is the counterpart to `PyArg_ParseTuple()`. It is declared as follows:





It recognizes a set of format units similar to the ones recognized by `PyArg_ParseTuple()`, but the arguments (which are input to the function, not output) must not be pointers, just values. It returns a new Python object, suitable for returning from a C function called from Python.

One difference with `PyArg_ParseTuple()`: while the latter requires its first argument to be a tuple (since Python argument lists are always represented as tuples internally), `Py_BuildValue()` does not always build a tuple. It builds a tuple only if its format string contains two or more format units. If the format string is empty, it returns `None`; if it contains exactly one format unit, it returns whatever object is described by that format unit. To force it to return a tuple of size 0 or one, parenthesize the format string.

When memory buffers are passed as parameters to supply data to build objects, as for the "`s`" and "`s#`" formats, the required data is copied. Buffers provided by the caller are never referenced by the objects created by `Py_BuildValue()`. In other words, if your code invokes `malloc()` and passes the allocated memory to `Py_BuildValue()`, your code is responsible for calling `free()` for that memory once `Py_BuildValue()` returns.

In the following description, the quoted form is the format unit; the entry in (round) parentheses is the Python object type that the format unit will return; and the entry in [square] brackets is the type of the C value(s) to be passed.

The characters space, tab, colon and comma are ignored in format strings (but not within format units such as "`s#`"). This can be used to make long format strings a tad more readable.



- **"s" (string) [char \*]**

    Convert a null-terminated C string to a Python object. If the C string pointer is `NULL`, `None` is used.

- **"s#" (string) [char \*, int]**

    Convert a C string and its length to a Python object. If the C string pointer is `NULL`, the length is ignored and `None` is returned.

- **"z" (string or None) [char \*]**

    Same as "`s`".

- **"z#" (string or None) [char \*, int]**

    Same as "`s#`".

- **"u" (Unicode string) [Py_UNICODE \*]**

    Convert a null-terminated buffer of Unicode (UCS-2) data to a Python Unicode object. If the Unicode buffer pointer is `NULL`, `None` is returned.

- **"u#" (Unicode string) [Py_UNICODE \*, int]**

    Convert a Unicode (UCS-2) data buffer and its length to a Python Unicode object. If the Unicode buffer pointer is `NULL`, the length is ignored and `None` is returned.

- **"u" (Unicode string) [Py_UNICODE \*]**

    Convert a null-terminated buffer of Unicode (UCS-2) data to a Python Unicode object. If the Unicode buffer pointer is `NULL`, `None` is returned.

- **"u#" (Unicode string) [Py_UNICODE \*, int]**

    Convert a Unicode (UCS-2) data buffer and its length to a Python Unicode object. If the Unicode buffer pointer is `NULL`, the length is ignored and `None` is returned.

- **"i" (integer) [int]**

    Convert a plain C `int` to a Python integer object.

- **"b" (integer) [char]**

    Same as "`i`".

- **"h" (integer) [short int]**

    Same as "`i`".

- **"l" (integer) [long int]**

    Convert a C `long int` to a Python integer object.

- **"c" (string of length 1) [char]**

    Convert a C `int` representing a character to a Python string of length 1.

- **"d" (float) [double]**

    Convert a C `double` to a Python floating point number.

- **"f" (float) [float]**

    Same as "`d`".

- **"O" (object) [PyObject \*]**

    Pass a Python object untouched (except for its reference count, which is incremented by one). If the object passed in is a `NULL` pointer, it is assumed that this was caused because the call producing the argument found an error and set an exception. Therefore, `Py_BuildValue()` will return `NULL` but won't raise an exception. If no exception has been raised yet, `PyExc_SystemError` is set.

- **"S" (object) [PyObject \*]**

    Same as "`O`".

- **"U" (object) [PyObject \*]**

    Same as "`O`".

- **"N" (object) [PyObject \*]**

    Same as "`O`", except it doesn't increment the reference count on the object. Useful when the object is created by a call to an object constructor in the argument list.

- **"O&" (object) [converter, anything]**

    Convert anything to a Python object through a converter function. The function is called with anything (which should be compatible with `void *`) as its argument and should return a ``new'' Python object, or `NULL` if an error occurred.

- **"(items)" (tuple) [matching-items]**

    Convert a sequence of C values to a Python tuple with the same number of items.

- **"[items]" (list) [matching-items]**

    Convert a sequence of C values to a Python list with the same number of items.

- **"{items}" (dictionary) [matching-items]**

    Convert a sequence of C values to a Python dictionary. Each pair of consecutive C values adds one item to the dictionary, serving as key and value, respectively.

If there is an error in the format string, the `PyExc_SystemError` exception is raised and `NULL` returned.

Examples (to the left the call, to the right the resulting Python value):








